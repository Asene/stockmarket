FROM openjdk:8-jdk-alpine
VOLUME /tmp
EXPOSE 8080
ADD target/stockmarket-0.0.1-SNAPSHOT.jar stockmarket.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-Dspring.profiles.active=container", "-jar", "/stockmarket.jar"]
