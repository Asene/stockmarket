package com.example.stockmarket.controlleur;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.stockmarket.model.DataSet;
import com.example.stockmarket.service.RestService;

@Controller
@RequestMapping(path = "/stockmarket")
@ResponseBody
public class RestController {
    private final RestService restService;
    
    /*
     * commentaire ici
     */
    @Autowired
    public RestController(RestService restService) {
        this.restService = restService;
    }
    
    /*
     * commentaire ici
     */
    @RequestMapping(value = "/id", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity findById(@RequestParam(value = "id") String id){
        Long Id = Long.parseLong(id);
        return ResponseEntity.ok(restService.getDataSet(Id));
    }
    
    /*
     * commentaire ici
     */
    @RequestMapping(value = "/area", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity findByArea(@RequestParam(value = "area") String area){
        return ResponseEntity.ok(restService.getDataSetByArea(area));
    }
    
    /*
     * commentaire ici
     */
    @GetMapping(value = "/lister", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity findAll(){
        return ResponseEntity.ok(restService.getAllDataSet());
    }
    
    /*
     * commentaire ici
     */
    @PostMapping(value ="/add", produces = MediaType.APPLICATION_JSON_VALUE)
    DataSet save(@RequestBody DataSet dataSet) {
        return restService.addDataSet(dataSet);
    }
}