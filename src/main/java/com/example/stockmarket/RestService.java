package com.example.stockmarket;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class RestService {
    private DataSetRepository dateSetRepository;

    @Autowired
    public RestService(DataSetRepository dateSetRepository) {
        this.dateSetRepository = dateSetRepository;
    }

    public DataSet getDataSet(Long id){
        DataSet dataSet= dateSetRepository.findById(id);
        //String result="{ID : "+dataSet.getId().toString()+",numberOfInstances : "+dataSet.getNumberOfInstances()+",dataSetCharacteristics :"+ book.getAuthor()+" }";
        return dataSet;
    }
    
    public  List<DataSet> getAllDataSet(){
        return  dateSetRepository.findAll();
    }

    public  DataSet addDataSet(DataSet dataSet){
        return  dateSetRepository.save(dataSet);
    }
}