package com.example.stockmarket.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "data_set")
public class DataSet {

    @Id
    @Column(name="ID")
    private Long id;
    
    @Column(name="NUMBER_INSTANCES")
    private Long numberOfInstances;
    
    @Column(name="DATA_CHARACT")
    private String dataSetCharacteristics;
    
    @Column(name="AREA")
    private String area;


    @Column(name="DATE_DONATED")
    private Date dateDonated;


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Long getNumberOfInstances() {
		return numberOfInstances;
	}


	public void setNumberOfInstances(Long numberOfInstances) {
		this.numberOfInstances = numberOfInstances;
	}


	public String getDataSetCharacteristics() {
		return dataSetCharacteristics;
	}


	public void setDataSetCharacteristics(String dataSetCharacteristics) {
		this.dataSetCharacteristics = dataSetCharacteristics;
	}


	public String getArea() {
		return area;
	}


	public void setArea(String area) {
		this.area = area;
	}


	public Date getDateDonated() {
		return dateDonated;
	}


	public void setDateDonated(Date dateDonated) {
		this.dateDonated = dateDonated;
	}


}