package com.example.stockmarket;

import org.springframework.data.jpa.repository.JpaRepository;

public interface DataSetRepository  extends JpaRepository<DataSet,String> {

    DataSet findById(Long id);

}