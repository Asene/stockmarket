package com.example.stockmarket.service;

import com.example.stockmarket.model.DataSet;
import com.example.stockmarket.ripository.DataSetRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class RestService {
    private DataSetRepository dateSetRepository;
    
    /*
     * commentaire ici
     */
    @Autowired
    public RestService(DataSetRepository dateSetRepository) {
        this.dateSetRepository = dateSetRepository;
    }

    /*
     * commentaire ici
     */
    public DataSet getDataSet(Long id){
        DataSet dataSet= dateSetRepository.findById(id);
        return dataSet;
    }
    
    /*
     * commentaire ici
     */
    public List<DataSet> getDataSetByArea(String area){
        return dateSetRepository.findByArea(area);
    }
    
    /*
     * commentaire ici
     */
    public  List<DataSet> getAllDataSet(){
        return  dateSetRepository.findAll();
    }
    
    /*
     * commentaire ici
     */
    public  DataSet addDataSet(DataSet dataSet){
        return  dateSetRepository.save(dataSet);
    }
}