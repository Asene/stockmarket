package com.example.stockmarket.ripository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.stockmarket.model.DataSet;
/*
 * commentaire ici
 */
public interface DataSetRepository  extends JpaRepository<DataSet,String> {
    /*
     * commentaire ici
     */
    DataSet findById(Long id);
    
    /*
     * commentaire ici
     */
    @Query(value = "SELECT t FROM DataSet t WHERE t.area = ?1")
    List<DataSet> findByArea(String area);
}