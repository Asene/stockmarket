package com.example.stockmarket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/stockmarket")
@ResponseBody
public class RestController {
    private final RestService restService;

    @Autowired
    public RestController(RestService restService) {
        this.restService = restService;
    }
    @RequestMapping(value = "area", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity findById(@RequestParam(value = "id") String id){
        Long Id = Long.parseLong(id);
        return ResponseEntity.ok(restService.getDataSet(Id));
    }
    
    @GetMapping(value = "/lister", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity findAll(){
        return ResponseEntity.ok(restService.getAllDataSet());
    }

    @PostMapping(value ="/add", produces = MediaType.APPLICATION_JSON_VALUE)
    DataSet save(@RequestBody DataSet dataSet) {
        return restService.addDataSet(dataSet);
    }
}